package functionalTests;

import org.testng.annotations.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
//import org.openqa.selenium.support.ui.WebDriverWait;
import auxClasses.Browsers;
import auxClasses.ReadUrlFile;
import auxClasses.WindowHandler;


public class ejercicio1 extends Browsers {
	@Test
	
	public void f() throws Exception{
		driver.get("http://compras135.ufm.edu/");
		wait.until(ExpectedConditions.elementToBeClickable(By.name("btnLoginGoogle"))).click();
		login l = new login();
		l.log();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[contains(text(),'UFM Apps')]"))).click();

		wait.until(ExpectedConditions.elementToBeClickable((By.partialLinkText("Administración académica")))).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.id("tdItemMenu_4"))).click();
		
		driver.findElement(By.partialLinkText("Administración de LOGOS")).click();
		
		driver.findElement(By.id("buttClassListadoLogosNuevaSolicitud")).click();

		WindowHandler.next(driver);
		wait.until(ExpectedConditions.visibilityOfElementLocated(((By.id("txtNombre"))))).sendKeys("Karl Heinz Chávez Asturia");
		wait.until(ExpectedConditions.visibilityOfElementLocated(((By.id("txtCarne"))))).sendKeys("20100057");
		wait.until(ExpectedConditions.visibilityOfElementLocated(((By.id("sltLogoOrigen"))))).sendKeys("Catedrático");
		driver.findElement(By.cssSelector("button")).click();
		wait.until(ExpectedConditions.elementToBeClickable((By.id("tdSltLogoActividad"))));
		wait.until(ExpectedConditions.elementToBeClickable((By.xpath("//.//*[@id='tdSltLogoActividad']/div[1]/ul/li[2]/label/span")))).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(((By.id("txtNombreActividad"))))).sendKeys("TestXIK_Garcia");
		wait.until(ExpectedConditions.visibilityOfElementLocated(((By.id("txtDescripcion"))))).sendKeys("prueba xd xd xd");
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(((By.name("txtLogoAcredita"))))).sendKeys("420");
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(((By.name("txtLogoReposicion"))))).sendKeys("520");
		wait.until(ExpectedConditions.elementToBeClickable((By.xpath("//*[@id='divEditSubCiclo']/button")))).click();
		wait.until(ExpectedConditions.elementToBeClickable((By.xpath("//*[@id='divEditSubCiclo']/div/ul/li[3]/label/span")))).click();

		
		wait.until(ExpectedConditions.visibilityOfElementLocated(((By.name("txtCupo_1_1"))))).sendKeys("1");
		wait.until(ExpectedConditions.visibilityOfElementLocated(((By.name("txtSeccion_1_1"))))).sendKeys("A");
		
		driver.findElement(By.id("buttSendSolicitud")).click();
		
		driver.close();
		WindowHandler.goMain(driver);
		
		wait.until(ExpectedConditions.visibilityOfElementLocated(((By.name("txtListadoLogo_Titulo"))))).sendKeys("TestXIK_Garcia");
		driver.findElement(By.id("buttClassListadoLogosBuscar")).click();
		
		ReadUrlFile.Wait(10000);


	}
}
