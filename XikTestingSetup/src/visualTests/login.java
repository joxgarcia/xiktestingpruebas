package visualTests;

import org.sikuli.script.Region;
import org.sikuli.script.Screen;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;

public class login extends Browsers {
	@Test
	public void log() throws Exception{
		Screen screen = new Screen();
		Region email = screen.wait("img/email.png").highlight();
		email.click();
		email.click();
		email.type("xik@ufm.edu\n");
		
		ReadUrlFile.Wait(2000);
		Region pw = screen.wait("img/pw.png").highlight();
		pw.click();
		pw.click();
		pw.type("xik$2015");
		
		Region next = screen.wait("img/next.png").highlight();
		next.click();
		next.click();
	}

}
