package visualTests;

import org.sikuli.script.Region;
import org.sikuli.script.Screen;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;
public class ejercicio1 extends Browsers {
	@Test
	public void f() throws Exception{
		Screen screen = new Screen();


		driver.get("http://compras135.ufm.edu/");
		Region ingreso = screen.wait("img/ingresar.png").highlight();
		ingreso.click();
		ingreso.click();
		ReadUrlFile.Wait(5000);
		
		login l = new login();
		l.log();
		
		ReadUrlFile.Wait(8000);
		screen.wait("img/UFMAPPS.png").click();

		screen.wait("img/ac.png").click();
		
		ReadUrlFile.Wait(2000);
		
		Region ca = screen.wait("img/ca.png").highlight();
		ca.click();
		ca.click();
		
		Region al = screen.wait("img/al.png").highlight();
		al.click();
		al.click();
		
		ReadUrlFile.Wait(2000);
		screen.wait("img/ns.png").click();

		Region nombre = screen.wait("img/nombrefacilitador.png");
		nombre.click();
		nombre.type("Karl Heinz Chavez Asturia");
		ReadUrlFile.Wait(5000);
		screen.wait("img/select.png").click();
		ReadUrlFile.Wait(2000);
		screen.wait("img/cf.png").click();
		ReadUrlFile.Wait(2000);
		screen.wait("img/cat.png").click();
		ReadUrlFile.Wait(2000);
		screen.wait("img/titulo.png").click();
		ReadUrlFile.Wait(2000);
		screen.wait("img/nuevologo.png").click();
		Region texto = screen.wait("img/text.png");
		texto.click();
		texto.type("TestXIK_Garcia");
		ReadUrlFile.Wait(2000);
		Region desc = screen.wait("img/descp.png");
		desc.click();
		desc.type("prueba");
		ReadUrlFile.Wait(2000);
		Region cant = screen.wait("img/cantlog.png");
		cant.click();
		cant.type("2");
		ReadUrlFile.Wait(2000);
		Region rep = screen.wait("img/reposicion.png");
		rep.click();
		rep.type("2");
		ReadUrlFile.Wait(2000);	
		screen.wait("img/ciclo.png").click();
		screen.wait("img/ciclosel.png").click();
		Region cu = screen.wait("img/cupo.png");
		cu.click();
		cu.type("2");
		ReadUrlFile.Wait(2000);	
		Region sec = screen.wait("img/seccion.png");
		sec.click();
		cu.type("A");
		ReadUrlFile.Wait(2000);	
		
		screen.wait("img/es.png").click();
		ReadUrlFile.Wait(5000);	

		Region ca2 = screen.wait("img/ca.png").highlight();
		ca2.click();
		ca2.click();
		
		Region al2 = screen.wait("img/al.png").highlight();
		al2.click();
		al2.click();
		
		Region tit = screen.wait("img/titulo2.png");
		tit.click();
		tit.type("TestXIK_Garcia");
		screen.wait("img/buscar.png").click();
		ReadUrlFile.Wait(10000);	

	}
}
