package functionalTests;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;
import auxClasses.WindowHandler;

public class prueba2 extends Browsers {
	@Test
	public void f() throws Exception{
		WebDriverWait wait = new WebDriverWait(driver, 10);
		driver.get("http://compras135.ufm.edu/MiU");
		wait.until(ExpectedConditions.elementToBeClickable(By.id("miu_"))).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("identifierId"))).sendKeys("xik@ufm.edu\n");

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("password"))).sendKeys("xik$2015");
		wait.until(ExpectedConditions.elementToBeClickable(By.id("passwordNext"))).click();


		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("divContentImage_7"))).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='divContentdivFecla_7']/table/tbody/tr[5]/td[2]"))).click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("txtSeleccionUsuario"))).sendKeys("20080633");
		ReadUrlFile.Wait(5000);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("ui-id-4"))).click();
		ReadUrlFile.Wait(5000);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("bttSearch"))).click();
		ReadUrlFile.Wait(10000);

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("divContentImage_3"))).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='divContentdivFecla_3']/table/tbody/tr[2]/td[2]"))).click();
		
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(@onclick,'102040')]"))).click();
		WindowHandler.next(driver);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(@onclick,'fntCallModalAdminTarea')]"))).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("txtAdminTareaTitulo"))).sendKeys("Tarea prueba");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("editor"))).sendKeys("esto es una prueba");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("txtAdminTareaFechaInicio_dia"))).sendKeys("22");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("txtAdminTareaFechaInicio_mes"))).sendKeys("06");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("txtAdminTareaFechaInicio_anio"))).sendKeys("2017");
		wait.until(ExpectedConditions.elementToBeClickable(By.name("txtAdminTareaHoraInicioHora"))).sendKeys("7");
		wait.until(ExpectedConditions.elementToBeClickable(By.name("lstAdminTareaTipoNotaMin"))).sendKeys("0");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("txtAdminTareaFechaLimite_dia"))).sendKeys("23");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("txtAdminTareaFechaLimite_mes"))).sendKeys("06");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("txtAdminTareaFechaLimite_anio"))).sendKeys("2017");
		wait.until(ExpectedConditions.elementToBeClickable(By.name("txtAdminTareaHoraLimiteHora"))).sendKeys("23");
		wait.until(ExpectedConditions.elementToBeClickable(By.name("txtAdminTareaHoraLimiteMin"))).sendKeys("55");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("txtAdminTareaLugar"))).sendKeys("casa");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("textareaAdminTareaObservaciones"))).sendKeys("es una prueba");
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("txtAdminTareaNota"))).sendKeys("10");
		wait.until(ExpectedConditions.elementToBeClickable(By.id("btnSaveTarea"))).click();
		driver.close();
		WindowHandler.goMain(driver);

		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/header/div/div[2]/div[3]/table/tbody/tr[3]/td/div/input"))).click();
		
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("divContentImage_7"))).click();
		//wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='divContentdivFecla_7']/table/tbody/tr[5]/td[2]"))).click();

		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("txtSeleccionUsuario"))).sendKeys("20130776");
		ReadUrlFile.Wait(5000);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("ui-id-4"))).click();
		ReadUrlFile.Wait(5000);
		wait.until(ExpectedConditions.elementToBeClickable(By.id("bttSearch"))).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("divContentImage_3"))).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='divContentdivFecla_3']/table/tbody/tr[2]/td[2]"))).click();
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(@onclick,'102040')]"))).click();
		WindowHandler.next(driver);
		ReadUrlFile.Wait(10000);
		driver.close();
		WindowHandler.goMain(driver);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("html/body/header/div/div[2]/div[3]/table/tbody/tr[3]/td/div/input"))).click();


		
		
		

	}
}